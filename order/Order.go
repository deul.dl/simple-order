package order

type Order struct {
	Id          int64 `gorm:"primary_key"`
	CustomerID  int64
	ExecutorID  int64
	Description string
	IsPickUp    bool
	Address     string
	Type        string // Home/Choose Product/Order
	Status      string //done/cookin
	WorkPrice   float64
}

type Orders []int64

func (Order) TableName() string {
	return "orders"
}
