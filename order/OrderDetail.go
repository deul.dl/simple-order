package order

type OrderDetail struct {
	ProductID int64
	OrderID   int64
}

type OrderDetails []OrderDetail
