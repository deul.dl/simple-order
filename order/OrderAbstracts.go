package order

type OrderFactory interface {
	Create(order *Order) error
}

type orderAbstract struct {
	OrderFactory
}
