package order

import (
	"fmt"
	"testing"
)

func TestCreateOnHomeSuccess(t *testing.T) {

	order := &OrderOnHome{
		Order: Order{
			CustomerID:  1,
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Address:     "Topar city",
			Type:        "Othrer",
			Status:      "start",
			WorkPrice:   100,
		},
	}

	err := New().CreateOnHome(order)
	fmt.Println(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateOnHomeWithDishSuccess(t *testing.T) {

	order := &OrderOnHome{
		Order: Order{
			CustomerID:  1,
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Address:     "Topar city",
			Type:        "Othrer",
			Status:      "start",
			WorkPrice:   100,
		},
		Dishes: Dishes{
			{Name: "22e3"},
			{Name: "22e3"},
		},
	}

	err := New().CreateOnHome(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateOnHomeFailed(t *testing.T) {

	order := &OrderOnHome{
		Order: Order{},
	}

	err := New().CreateOnHome(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateOnHomeWithDishFailed(t *testing.T) {

	order := &OrderOnHome{
		Order: Order{
			CustomerID:  1,
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Address:     "Topar city",
			Type:        "Othrer",
			Status:      "start",
			WorkPrice:   100,
		},
		Dishes: Dishes{},
	}

	err := New().CreateOnHome(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}
