package order

type proccessesOrder struct {
	proccessCreate       *createOrder
	proccessCreateOther  *createOrderOther
	proccessCreateOnHome *createOnHome
	proccessBuyProduct   *createOrderWithProduct
	proccessComplete     *completeOrder
}

func (this *proccessesOrder) GetProccesses() {
	this.getProccessCreate()
	this.getProccessCreateOther()
	this.getProccessComplete()
	this.getProccessCreateOnHome()
	this.getProccessBuyProduct()
}

func (this *proccessesOrder) getProccessCreate() {
	this.proccessCreate = newProccessCreate()
}

func (this *proccessesOrder) getProccessCreateOther() {
	this.proccessCreateOther = newProccessCreateOther()
}

func (this *proccessesOrder) getProccessCreateOnHome() {
	this.proccessCreateOnHome = newProccesssCreateOnHome()
}

func (this *proccessesOrder) getProccessBuyProduct() {
	this.proccessBuyProduct = newProccessBuy()
}

func (this *proccessesOrder) getProccessComplete() {
	this.proccessComplete = newProccessComplete()
}

func (this proccessesOrder) close(orderId int64) (err error) {

	//payment.returnCash
	return
}
