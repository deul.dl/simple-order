package order

type Dish struct {
	Id        int64 `gorm:"primary_key"`
	Name      string
	WorkPrice float64
	OrderId   int64
}

type Dishes []Dish

func (Dish) TableName() string {
	return "dishes"
}
