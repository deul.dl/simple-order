package order

import (
	"log"

	"errors"
)

type createOnHome struct {
}

func newProccesssCreateOnHome() *createOnHome {
	return &createOnHome{}
}

func (this createOnHome) start(order *OrderOnHome) (err error) {
	if err = this.check(order); err != nil {
		log.Println(err)
		return
	}

	this.save(order)

	return
}

func (this createOnHome) check(order *OrderOnHome) (err error) {
	if order.CustomerID == 0 {
		return errors.New("CustomerID")
	}

	if order.Address == "" {
		return errors.New("Address")
	}

	return
}

func (this createOnHome) save(order *OrderOnHome) {
}
