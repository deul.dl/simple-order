package order

type OrderWithProduct struct {
	Order
	OrderDetails OrderDetails
}
