package order

import (
	"testing"
)

func TestCreateOrderSuccess(t *testing.T) {

	orderData := &Order{
		CustomerID:  1,
		ExecutorID:  1,
		Description: "...",
		IsPickUp:    true,
		Address:     "Topar city",
		Type:        "Othrer",
		Status:      "start",
	}

	err := New().Create(orderData)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateOrderFailde(t *testing.T) {
	orderData := &Order{
		ExecutorID:  1,
		Description: "....",
		IsPickUp:    true,
		Type:        "Othrer",
		Status:      "start",
	}

	err := New().Create(orderData)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateWithProductsSuccess(t *testing.T) {

	order := &OrderWithProduct{
		Order: Order{
			CustomerID:  1,
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Address:     "Topar city",
			Type:        "buy",
			Status:      "start",
		},
		OrderDetails: OrderDetails{
			{1, 2},
			{1, 2},
		},
	}

	err := New().BuyProduct(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateWithProductsFailed(t *testing.T) {
	order := &OrderWithProduct{
		Order: Order{
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Type:        "buy",
			Status:      "start",
		},
		OrderDetails: OrderDetails{
			{1, 2},
			{1, 2},
		},
	}

	err := New().BuyProduct(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateWithProductsArrayFailed(t *testing.T) {
	order := &OrderWithProduct{
		Order: Order{
			CustomerID:  1,
			ExecutorID:  1,
			Description: "...",
			IsPickUp:    true,
			Address:     "Topar city",
			Type:        "buy",
			Status:      "start",
		},
	}

	err := New().BuyProduct(order)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}
