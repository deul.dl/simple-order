package order

import (
	"fmt"
	"log"
	"sync"

	"errors"
)

type createOrderOther struct {
}

func newProccessCreateOther() *createOrderOther {
	return &createOrderOther{}
}

func (this createOrderOther) start(order *OrderOther) (err error) {
	if err = this.check(order); err != nil {
		log.Println(err)
		return
	}

	this.save(order)

	return
}

func (this createOrderOther) check(order *OrderOther) (err error) {
	if order.CustomerID == 0 {
		return errors.New("CustomerID")
	}

	if order.Address == "" {
		return errors.New("Address")
	}

	if order.WorkPrice == 0 {
		return errors.New("WorkPrice")
	}

	if len(order.Dishes) == 0 {
		return errors.New("Dishes")
	}

	return
}

func (this createOrderWithProduct) checkDishes(order *OrderOther) (err error) {
	mutex := &sync.Mutex{}
	for _, dishes := range order.Dishes {
		go func() {
			mutex.Lock()
			if dishes.Name == "" {
				err = errors.New("Dishes")
			}
			mutex.Unlock()
		}()
	}

	return err
}

func (this createOrderOther) save(order *OrderOther) {
	fmt.Println(order)
	//this.store.SaveOrderOther(order)
}
