package order

type OrderOther struct {
	Id          int64 `gorm:"primary_key"`
	CustomerID  int64
	ExecutorID  int64
	Description string
	IsPickUp    bool
	Address     string
	Type        string
	Status      string
	WorkPrice   float64
	Dishes      Dishes `gorm:"foreignkey:OrderId"`
}

func (OrderOther) TableName() string {
	return "orders"
}
