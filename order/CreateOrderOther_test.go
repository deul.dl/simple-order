package order

import (
	"testing"
)

func TestCreateOrderOtherSuccess(t *testing.T) {

	orderOtherData := &OrderOther{
		CustomerID:  1,
		Description: "...",
		IsPickUp:    true,
		Address:     "Topar city",
		Type:        "Othrer",
		Status:      "start",
		WorkPrice:   100,
		Dishes: Dishes{
			{Name: "Name 1"},
			{Name: "Name 2"},
		},
	}

	err := New().CreateOther(orderOtherData)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}

func TestCreateOrderOtherFailed(t *testing.T) {

	orderOtherData := &OrderOther{
		CustomerID:  1,
		Description: "...",
		IsPickUp:    true,
		Address:     "Topar city",
		Type:        "Othrer",
		Status:      "start",
		WorkPrice:   100,
	}

	err := New().CreateOther(orderOtherData)
	if err != nil {
		t.Errorf("one of fiels is empty")
	}
}
