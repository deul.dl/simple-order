package order

import (
	"log"
)

type completeOrder struct{}

func newProccessComplete() *completeOrder {
	proccess := &completeOrder{}
	return proccess
}

func (this completeOrder) start(orderId int64) (err error) {
	if err = this.isOrder(orderId); err != nil {
		log.Println(err)
		return
	}

	this.save()

	return
}

func (this completeOrder) isOrder(order int64) (err error) {
	return
}

func (this completeOrder) save() {
}
