package order

import (
	"log"

	"errors"
)

type createOrder struct {
}

func newProccessCreate() *createOrder {
	return &createOrder{}
}

func (this createOrder) start(order *Order) (err error) {
	if err = this.check(order); err != nil {
		log.Println(err)
		return
	}

	this.save(order)

	return
}

func (this createOrder) check(order *Order) (err error) {
	if order.CustomerID == 0 {
		return errors.New("CustomerID")
	}

	if order.Address == "" {
		return errors.New("Address")
	}

	return
}

func (this createOrder) save(order *Order) {
	this.save(order)
}
