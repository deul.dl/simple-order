package order

type orderFactory struct {
	orderAbstract

	proccessesOrder
}

func New() *orderFactory {
	iterator := &orderFactory{}
	iterator.orderAbstract.OrderFactory = iterator
	iterator.GetProccesses()
	return iterator
}

func (this orderFactory) Create(order *Order) error {
	return this.proccessCreate.start(order)
}

func (this orderFactory) CreateOther(order *OrderOther) error {
	return this.proccessCreateOther.start(order)
}

func (this orderFactory) CreateOnHome(order *OrderOnHome) error {
	return this.proccessCreateOnHome.start(order)
}

func (this orderFactory) BuyProduct(order *OrderWithProduct) error {
	return this.proccessBuyProduct.start(order)
}

func (this orderFactory) Compete(orderId int64) error {
	return this.proccessComplete.start(orderId)
}

func (this orderFactory) Close(orderId int64) error {
	return this.close(orderId)
}
