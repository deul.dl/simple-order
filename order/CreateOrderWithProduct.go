package order

import (
	"log"
	"sync"

	"errors"
)

type createOrderWithProduct struct {
}

func newProccessBuy() *createOrderWithProduct {
	proccess := &createOrderWithProduct{}
	return proccess
}

func (this createOrderWithProduct) start(order *OrderWithProduct) (err error) {
	if err = this.check(order); err != nil {
		log.Println(err)
		return
	}

	this.save(order)

	return
}

func (this createOrderWithProduct) check(order *OrderWithProduct) (err error) {
	if order.CustomerID == 0 {
		return errors.New("CustomerID")
	}

	if order.Address == "" {
		return errors.New("Address")
	}

	if len(order.OrderDetails) == 0 {
		return errors.New("OrderDetauls is empty")
	}

	err = this.checkProductId(order)

	return
}

func (this createOrderWithProduct) checkProductId(order *OrderWithProduct) (err error) {
	mutex := &sync.Mutex{}
	for _, orderDetail := range order.OrderDetails {
		go func() {
			mutex.Lock()
			if orderDetail.ProductID == 0 {
				err = errors.New("ProductID")
			}
			mutex.Unlock()
		}()
	}

	return err
}

func (this createOrderWithProduct) save(order *OrderWithProduct) {
}
